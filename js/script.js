var landingPage = function(){

    var smoothScroll = function() {
      $('a.smothscroll').click(function(){
        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 500);
        return false;
      });
      // $(document).ready(function(){
      //       $(this).scrollTop(0);
      // });
    };

    var headerNav = function() {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 1){  
              $('#animatedheader').addClass("navbar-shrink");
            }
            else {
              $('#animatedheader').removeClass("navbar-shrink");
            }
          });
          $(document).ready(function docReady () {
            $('.navbar-collapse ul li a').click(function() {
              $('.navbar-toggle:visible').click();
            });
          });
    };

    var fitur = function () {
      $(".fitur-carousel").owlCarousel({
        items: 1,
        loop: false,
        nav: true,
        dots: false
      });
    };

    var gallery = function () {
      $(".albery-container").albery({
        speed: 500,
        imgWidth: 700,
        paginationBorder: 20,
	      paginationItemWidth: 280
      });
    };

    var tabsCycle = function () {
        var tabChange = function () {
            var tabs = $('#tab-cycle > li');
            var active = tabs.filter('.active');
            var next = active.next('li').length ? active.next('li').find('a') : tabs.filter(':first-child').find('a');

            next.tab('show');
        };

        var tabCycle = setInterval(tabChange, 8000);
    };

    return {
        init: function() {
            headerNav();
            smoothScroll();
            tabsCycle();
            fitur();
            gallery();
        }
    };
}();

$(document).ready(function() {
    landingPage.init();
    $(this).scrollTop(0);
});